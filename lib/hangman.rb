class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(option)
    @guesser = option[:guesser]
    @referee = option[:referee]
    @board = []
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length, '')
  end

  def take_turn
    guess = @guesser.guess
    position = @referee.check_guess(guess)
    update_board
    @guesser.handle_response(guess,position)
  end

  def update_board

  end


end

class HumanPlayer

end

class ComputerPlayer
  attr_accessor :secret_length, :dict, :board, :secret_word
  def initialize(dictionary)
    @dict = dictionary
    @board = []
  end

  def pick_secret_word
    @secret_word = @dict[rand(@dict.length-1)]
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @board = Array.new(length, nil) if @board.empty?
    @dict = @dict.select { |word| word.length == @secret_length }
  end

  def guess(board = @board)
    common = Hash.new(0)
    candidate_words
    board.each_with_index do |el, idx|
      next unless el.nil?
      @dict.each do |word|
        common[word[idx]] += 1
      end
    end
    most = common.values.max
    common.select { |_, v| v == most}.keys.first
  end

  def check_guess(letter)
    indices = []
    secret_word_chars = @secret_word.chars
    secret_word_chars.each_with_index do |char, idx|
      indices << idx if letter == char
    end
    indices
  end

  def handle_response(guess, position)
    @dict = @dict.reject { |word| word.include?(guess)} if position.empty?
    position.each do |idx|
      @board[idx] = guess
    end
  end

  def candidate_words
    candidate = []
    nil_cnt = @board.count(nil)
    unless nil_cnt.zero?
      @dict.each do |word|
        cnt = 0
        word.length.times do |n|
          cnt += 1 if word[n] == @board[n] && word.count(@board[n]) == @board.count(@board[n])
        end
        candidate << word if cnt == word.length - nil_cnt
      end
    end
    @dict = candidate
  end
end
